# Wardrobify

Team:

* Person 1 - Linh Ngo (shoes)
* Person 2 - Mark Pastoral (Hats)


## Design

## Shoes microservice

The wardrobe microservice manages data from the two models with various attributes; BinVO for storage information of shoes and for Shoes for information of various shoes.

The Shoe model includes attributes such as manufacturer, model name, color, the picture url, and a bin foreign key.

The BinVO it is also a storage container with attributes such as close name, bin number, bin size, and import href; all of which would be defined in the wardrobe microservice.

Each microservice (wardrobe, practice-shoe, practice-hat) is responsible for its own data stored within its models, interaction only occur through the APIs. At that point the APIs allows manipulation of data such as (CRUD) creating, reading, updating, and deleting information. Once the models are intergrated it would need information from the frontend to receive the data.

## Hats microservice

## Django
Connection to the Wardrobe microservice:
The 'wardrobe' microservice has the models Location and Bin. The microservice 'hats' connects with the Location model by creating a "copy" called LocationVO in its models.py file. The poller file in the hats microservice imports the LocationVO file, and uses a function "get_location" which gets data from the url, converts it to JSON. This way, if a location is made (which uses the models and views in the wardrobe microservice), the hats microservice can use the location as a property of the hats model.

Hat Models:
Besides the LocationVO, the 'Hats' model has several properties such as fabric and cloth, and location as a foreign key.

Hat Views:
The LocationVOEncoder, HatsListEncoder and HatsDetailEncoder are used to make sure the information from their models' properties is JSON readable. The list_hat view will get current existing instances of hats in the database and return a json-readable list of hats in a GET request. In a POST request, the user will be able to create an instance of a hat class in a form (representated by HatsForm.js).

The show_hat view will show all the properties of a single hat in a GET request, and allows for editting using PUT or deleting using DELETE. The DELETE is a requirement, and returns basic JSON text to confirm if the hat is deleted or not.

## React
HatsList:
Makes a table with the properties as headers and rows representing each instance of hat. 
The function "HatsList" will: 
- use a function "fetchData" to get data from the url 'http://localhost:8090/api/hats/'
- assign data to a variable "hatList" defined by the useStateHook
- make a function "handleDelete"
- return a table that uses .map to show each hat's data in a row
- fulfills the DELETE button requirement

HatForm:
- Defines all the hat model properties using useState
- uses fetchData to grab data from the wardrobe locations api
- defines several 'handle' functions that enable the html form section to have the user enter text.



