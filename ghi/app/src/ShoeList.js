import React, {useEffect, useState} from 'react';

function ShoeList(props) {

    const[shoe, setShoe] = useState([]);

    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (response.ok) {
            const data = await response.json();
            setShoe(data.shoes);
        }
    }


    const handleDelete = async (id) => {
        const response = await fetch (`http://localhost:8080/api/shoes/${id}/`, {
            method: "DELETE",

        });

        if (response.ok) {
            fetchData()
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

        return (
            <table className="table table-hover table-striped border border-5">
                <thead>
                    <tr>
                        <th>Picture</th>
                        <th>Manufacturer</th>
                        <th>Model Name</th>
                        <th>Color</th>
                        <th>Bin Name</th>
                    </tr>
                </thead>
                <tbody>
                    {shoe?.map(shoe => {
                    {/* {props.shoe?.map(shoes => { */}
                    return (
                            <tr key={shoe.id}>
                                <td><img src={shoe.picture_url} width="100" height="100" />{}</td>
                                <td>{ shoe.manufacturer }</td>
                                <td>{ shoe.model }</td>
                                <td>{ shoe.color }</td>
                                <td>{ shoe.bin}</td>
                                <td>
                                    <button className="btn btn-light" onClick={() => handleDelete(shoe.id)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        );
}

export default ShoeList;
