import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatForm from './HatForm';
import ShoeForm from './ShoeForm';
import ShoeList from './ShoeList';

function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats">
            <Route path="" element={<HatsList />} />
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="/shoes">
            <Route path="new" element={<ShoeForm />} />
            <Route path="" element={<ShoeList />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
