import { useEffect , useState} from "react";

function HatForm() {
    const[styleName, setStyleName] = useState('')
    const[color, setColor] = useState('')
    const[fabric, setFabric] = useState('')
    const[pictureUrl, setPictureUrl] = useState('')
    const[location, setLocation] = useState('')
    const[locations, setLocations] = useState([])

    const fetchData = async() => {
        const url = 'http://localhost:8100/api/locations/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    const handleFabricChange = async (event) => {
        const value = event.target.value
        setFabric(value)
    }

    const handleColorChange = async (event) => {
        const value = event.target.value
        setColor(value)
    }

    const handleStyleNameChange = async (event) => {
        const value = event.target.value
        setStyleName(value)
    }

    const handlePictureUrlChange = async (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }

    const  handleLocationChange = async (event) => {
        const value = event.target.value
        setLocation(value)
    }

    const handleSubmit = async(event) => {
        event.preventDefault()
        const data = {}
            data.style_name = styleName
            data.color = color
            data.fabric = fabric
            data.picture_url = pictureUrl
            data.location = location
        console.log(data)

        const hatUrl = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type':'application/json',
            }
        }
        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok) {
            const newHat = await response.json()
            console.log(newHat)
            setStyleName('')
            setColor('')
            setFabric('')
            setPictureUrl('')
            setLocation('')

        } else {
            console.log("owo it isn't working")
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input value={styleName} onChange={handleStyleNameChange} placeholder="style name" required type="text" name="styleName" id="styleName" className="form-control" />
                            <label htmlFor="styleName">Style Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={fabric} onChange={handleFabricChange} placeholder="fabric" required type="text" name="city" id="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div>
                            <input value={pictureUrl} onChange={handlePictureUrlChange} placeholder="picture url" required type="url" id="pictureUrl" className="form-control" />
                            <label htmlFor="pictureUrl"></label>
                        </div>
                        <div className="mb-3">
                            <select value={location} onChange={handleLocationChange} placeholder="location" required name="location" id="location" className="form-control">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.closet_name}
                                        </option>)
                                    })}
                                    </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    )
}


export default HatForm